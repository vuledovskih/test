package ru.home.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.home.service.api.resource.ProductResource;
import ru.home.service.service.ProductService;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RequestMapping("search")
@RestController
@PreAuthorize("hasRole('USER')")
public class SearchController {

    @Autowired
    private ProductService service;

    @GetMapping("brand/{brand}")
    public ResponseEntity<List<ProductResource>> findByBrand(@PathVariable("brand") String brand) {
        return ok(service.findByBrand(brand));
    }

    @GetMapping("name/{name}")
    public ResponseEntity<List<ProductResource>> findByName(@PathVariable("name") String name) {
        return ok(service.findByName(name));
    }

}
