package ru.home.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.home.service.api.resource.ProductResource;
import ru.home.service.api.resource.ProductResourceAssembler;
import ru.home.service.domain.repositories.ProductRepository;
import ru.home.service.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/")
public class LeftoverController {

    @Autowired
    private ProductService service;

    @GetMapping("leftover")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<List<ProductResource>> leftover() {
        return ok(service.loadLeftover());
    }

}
