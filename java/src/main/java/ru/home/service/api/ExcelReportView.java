package ru.home.service.api;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import ru.home.service.domain.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExcelReportView extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response) {

        response.setHeader("Content-Disposition", "attachment;filename=\"products.xls\"");
        Sheet sheet = workbook.createSheet("Product List (" + map.get("query") + ")");
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Brand");
        header.createCell(1).setCellValue("Name");
        header.createCell(2).setCellValue("Quantity");
        header.createCell(3).setCellValue("Price");

        int rowNum = 1;
        List<Product> productList = (List<Product>) map.get("list");

        for (Product product : productList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(product.getBrand());
            row.createCell(1).setCellValue(product.getName());
            row.createCell(2).setCellValue(product.getQuantity());
            row.createCell(3).setCellValue(product.getPrice().doubleValue());
        }
    }
}
