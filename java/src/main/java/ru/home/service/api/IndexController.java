package ru.home.service.api;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/")
public class IndexController {

    @GetMapping("index")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResourceSupport> index() {
        ResourceSupport index = new ResourceSupport();
        index.add(
                linkTo(methodOn(IndexController.class).index()).withSelfRel(),
                linkTo(methodOn(LeftoverController.class).leftover()).withRel("product leftover list"),
                linkTo(methodOn(SearchController.class).findByBrand("{brand}")).withRel("find products by brand"),
                linkTo(methodOn(SearchController.class).findByName("{name}")).withRel("find products by name")
        );

        return ok(index);
    }
}
