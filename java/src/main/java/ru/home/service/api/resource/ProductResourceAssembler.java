package ru.home.service.api.resource;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import ru.home.service.api.ProductController;
import ru.home.service.domain.Product;

public class ProductResourceAssembler extends ResourceAssemblerSupport<Product, ProductResource> {
    public ProductResourceAssembler() {
        super(ProductController.class, ProductResource.class);
    }

    @Override
    public ProductResource toResource(Product entity) {
        ProductResource resource = createResourceWithId(entity.getId(), entity);
        resource.setBrand(entity.getBrand());
        resource.setName(entity.getName());
        resource.setPrice(entity.getPrice());
        resource.setQuantity(entity.getQuantity());
        return resource;
    }
}
