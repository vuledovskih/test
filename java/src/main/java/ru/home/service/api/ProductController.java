package ru.home.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.home.service.api.resource.ProductResource;
import ru.home.service.service.ProductService;

import static org.springframework.http.HttpStatus.RESET_CONTENT;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("product")
@ExposesResourceFor(ProductResource.class)
public class ProductController {

@Autowired
private ProductService service;

    @GetMapping(path = "{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ProductResource> get(@PathVariable("id") String id) {
        return ok(service.getById(id));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductResource> put(@RequestBody ProductResource resource, @PathVariable String id) {
        ProductResource updated = service.update(resource, id);
        return updated != null ? ok(updated) : badRequest().build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductResource> create(@RequestBody ProductResource resource) {
        ProductResource updated = service.create(resource);
        return updated != null ? ok(updated) : badRequest().build();
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity delete(@PathVariable(value = "id") String id) {
        service.delete(id);
        return status(RESET_CONTENT).build();
    }

}
