package ru.home.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.home.service.domain.Product;
import ru.home.service.service.ReportService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report")
@PreAuthorize("hasRole('USER')")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/brand/{brand}")
    public ModelAndView byBrand(@PathVariable("brand") String brand) {
        return createView(reportService.listByBrand(brand), "find by brand " + brand);
    }

    @GetMapping("/name/{name}")
    public ModelAndView byName(@PathVariable("name") String name) {
        return createView(reportService.listByBrand(name), "find by name " + name);
    }

    private ModelAndView createView(List<Product> list, String query) {
        Map<String, Object> params = new HashMap<>();
        params.put("query", query);
        params.put("list", list);

        return new ModelAndView(new ExcelReportView(), params);
    }
}
