package ru.home.service.domain.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ru.home.service.domain.Product;

import java.util.List;

public interface ProductRepository extends MongoRepository<Product, String> {

    @Query("{quantity : {$lt : 5}}")
    List<Product> getLeftOver();

    List<Product> findProductsByBrand(String brand);

    List<Product> findProductsByName(String name);
}
