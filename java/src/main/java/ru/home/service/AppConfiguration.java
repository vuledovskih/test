package ru.home.service;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ru.home.service.api.resource.ProductResourceAssembler;

@Configuration
public class AppConfiguration {

//    @Autowired
//    private CustomizableTraceInterceptor controllerLog;

    @Bean
    public ProductResourceAssembler assembler() {
        return new ProductResourceAssembler();
    }

    @Bean
    public AopLogger aopLogger() {
        return new AopLogger();
    }

//    @Bean
//    public CustomizableTraceInterceptor controllerLog() {
//        CustomizableTraceInterceptor logger = new CustomizableTraceInterceptor();
//        logger.setEnterMessage("==> $[targetClassShortName].$[methodName]($[arguments])");
//        logger.setExceptionMessage("<== $[targetClassShortName].$[methodName]() execution failed: $[exception]");
//        logger.setExitMessage("<== $[targetClassShortName].$[methodName]() result: $[returnValue]");
//        return logger;
//    }
//
//    @Bean
//    public Advisor jpaRepositoryAdvisor(CustomizableTraceInterceptor interceptor) {
//        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
//        pointcut.setExpression("execution(public * ru.home.service.api.*.*(..))");
//        return new DefaultPointcutAdvisor(pointcut, interceptor);
//    }

}
