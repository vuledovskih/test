package ru.home.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.home.service.domain.Product;
import ru.home.service.domain.repositories.ProductRepository;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private final ProductRepository repository;

    @Autowired
    public ReportServiceImpl(ProductRepository repository) {
        Assert.notNull(repository, "repository empty");
        this.repository = repository;
    }

    @Override
    public List<Product> listByBrand(String brand) {
        return repository.findProductsByBrand(brand);
    }

    @Override
    public List<Product> listByName(String name) {
        return repository.findProductsByName(name);
    }
}
