package ru.home.service.service;

import ru.home.service.domain.Product;

import java.util.List;

public interface ReportService {

    List<Product> listByBrand(String brand);

    List<Product> listByName(String name);

}
