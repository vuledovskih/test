package ru.home.service.service;

import ru.home.service.api.resource.ProductResource;
import ru.home.service.domain.Product;

import java.util.List;

public interface ProductService {

    ProductResource update(ProductResource resource, String id);

    ProductResource create(ProductResource resource);

    void delete(String id);

    List<ProductResource> loadLeftover();

    ProductResource getById(String id);

    List<ProductResource> findByName(String name);

    List<ProductResource> findByBrand(String brand);
}
