package ru.home.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.home.service.api.resource.ProductResource;
import ru.home.service.api.resource.ProductResourceAssembler;
import ru.home.service.domain.Product;
import ru.home.service.domain.repositories.ProductRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;
    private final ProductResourceAssembler assembler;

    @Autowired
    public ProductServiceImpl(ProductRepository repository, ProductResourceAssembler assembler) {
        Assert.notNull(repository, "repository is empty");
        Assert.notNull(assembler, "assembler is empty");

        this.repository = repository;
        this.assembler = assembler;
    }

    @Override
    public ProductResource update(ProductResource resource, String id) {
        if (!repository.existsById(id)) {
            return null;
        }

        Product product = toProduct(resource);
        product.setId(id);
        return assembler.toResource(repository.save(product));
    }

    @Override
    public ProductResource create(ProductResource resource) {
        Product product = toProduct(resource);
        product.setId(UUID.randomUUID().toString());
        return assembler.toResource(repository.save(product));
    }

    private Product toProduct(ProductResource resource) {
        Product product = new Product();
        product.setQuantity(resource.getQuantity());
        product.setName(resource.getName());
        product.setBrand(resource.getBrand());
        product.setPrice(resource.getPrice());
        return product;
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }

    @Override
    public List<ProductResource> loadLeftover() {
        return toResource(repository.getLeftOver());
    }

    @Override
    public ProductResource getById(String id) {
        return repository.findById(id).map(assembler::toResource).orElse(new ProductResource());
    }

    @Override
    public List<ProductResource> findByName(String name) {
        return toResource(repository.findProductsByName(name));
    }

    @Override
    public List<ProductResource> findByBrand(String brand) {
        return toResource(repository.findProductsByBrand(brand));
    }

    private List<ProductResource> toResource(List<Product> products) {
        return products.stream().map(assembler::toResource).collect(Collectors.toList());
    }
}
