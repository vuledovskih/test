package ru.home.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@ComponentScan(basePackages = "ru.home.service")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ServiceApplication {

    public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}

}
