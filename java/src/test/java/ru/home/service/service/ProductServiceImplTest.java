package ru.home.service.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import ru.home.service.MockitoExtension;
import ru.home.service.api.resource.ProductResource;
import ru.home.service.api.resource.ProductResourceAssembler;
import ru.home.service.domain.Product;
import ru.home.service.domain.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    private static final String BOOTS = "boots";
    private static final String ASUS = "asus";

    @Mock
    private ProductRepository repository;

    @Spy
    private ProductResourceAssembler assembler = new ProductResourceAssembler();

    @InjectMocks
    private ProductServiceImpl service;


    @BeforeEach
    void setUp() {
        when(repository.findProductsByName(BOOTS)).thenReturn(singletonList(getProduct("nike", BOOTS)));
        when(repository.findProductsByBrand(ASUS)).thenReturn(singletonList(getProduct(ASUS, "memory")));
        when(repository.save(any())).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    void testCreate() {
        ProductResource resource = getProductResource();
        ProductResource result = assertProductEquals(resource, service.create(resource));

        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);

        verify(repository).save(captor.capture());
        verifyNoMoreInteractions(repository);
        Product product = captor.getValue();

        assertEquals(idToLinkString(product.getId()), result.getId().getHref());
    }

    @Test
    void updateExistProduct() {
        ProductResource oldResource = getProductResource();
        oldResource.setBrand("123");
        String id = "uuid_unique";
        ProductResource resource = getProductResource();
        when(repository.existsById(id)).thenReturn(true);

        ProductResource result = assertProductEquals(resource, service.update(resource, id));
        assertNotEquals(oldResource.getBrand(), result.getBrand());
        assertEquals(idToLinkString(id), result.getId().getHref());

        verify(repository).existsById(id);


        verify(repository).save(any());
        assertEquals(resource.getBrand(), result.getBrand());
        assertEquals(idToLinkString(id), result.getId().getHref());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void notFoundToUpdate() {
        ProductResource oldResource = getProductResource();
        String id = "uuid_unique";
        assertNull(service.update(oldResource, id));

        verify(repository).existsById(id);
        verifyNoMoreInteractions(repository);
    }

    private ProductResource getProductResource() {
        ProductResource resource = new ProductResource();
        resource.setQuantity(1);
        resource.setName(BOOTS);
        resource.setBrand(ASUS);
        return resource;
    }

    private ProductResource assertProductEquals(ProductResource expected, ProductResource actual) {
        assertAll(() ->{
            assertNotNull(actual);
            assertEquals(expected.getName(), actual.getName());
            assertEquals(expected.getBrand(), actual.getBrand());
            assertEquals(expected.getQuantity(), actual.getQuantity());
            assertEquals(expected.getPrice(), actual.getPrice());
        });
        return actual;
    }

    @Test
    void delete() {
        service.delete("ids");
        verify(repository, only()).deleteById("ids");
    }

    @Test
    void loadLeftover() {
        Product p = getProduct(ASUS, BOOTS);
        when(repository.getLeftOver()).thenReturn(Collections.singletonList(p));
        List<ProductResource> list = service.loadLeftover();
        assertNotNull(list);
        assertEquals(1, list.size());
        assertProductEquals(assembler.toResource(p), list.get(0));
    }

    @Test
    void testNoLeftover() {
        List<ProductResource> list = service.loadLeftover();
        assertNotNull(list);
        assertTrue(list.isEmpty());
        verify(repository, only()).getLeftOver();
    }

    @Test
    void getById() {
        Product p = getProduct("for_id", "get_by_id");
        String id = p.getId();
        when(repository.findById(id)).thenReturn(of(p));

        ProductResource result = service.getById(id);
        assertNotNull(result);
        assertEquals(idToLinkString(id), result.getId().getHref());
    }

    private String idToLinkString(String id) {
        return "/product/" + id;
    }

    @Test
    void findByName() {
        List<ProductResource> list = service.findByName(BOOTS);
        assertEquals(1, list.size());
        assertEquals(BOOTS, list.get(0).getName());
    }

    @Test
    void notFoundByName() {
        List<ProductResource> list = service.findByName(ASUS);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    void findByBrand() {
        List<ProductResource> list = service.findByBrand(ASUS);
        assertEquals(1, list.size());
        assertEquals(ASUS, list.get(0).getBrand());
    }

    @Test
    void notFoundByBrand() {
        List<ProductResource> list = service.findByBrand(BOOTS);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    private Product getProduct(String brand, String name) {
        Product product = new Product();

        product.setId(UUID.randomUUID().toString());
        product.setPrice(BigDecimal.TEN);
        product.setBrand(brand);
        product.setName(name);
        product.setQuantity(12);

        return product;
    }
}