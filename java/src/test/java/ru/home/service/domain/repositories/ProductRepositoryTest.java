package ru.home.service.domain.repositories;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.home.service.domain.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataMongoTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductRepositoryTest {
    private static final String ASUS = "ASUS";
    private static final String MOTHERBOARD = "Motherboard";
    private static final String NIKE = "NIKE";
    private static final String BOOTS = "BOOBS";


    @Autowired
    private ProductRepository repository;

    private Product single;

    @BeforeAll
    void setUp() {
        single = getProduct(1, "single", "single");
        repository.save(single);
        repository.save(getProduct(7, ASUS, MOTHERBOARD));
        repository.save(getProduct(9, NIKE, BOOTS));
    }

    @Test()
    void testGetLeftOver() {
        List<Product> result = repository.getLeftOver();
        assertNotNull(result);
        assertEquals(1, result.size());
        Product leftover = result.get(0);
        assertEquals(single.getId(), leftover.getId());
    }

    @Test
    void testFindProductsByBrand() {
        List<Product> result = repository.findProductsByBrand(NIKE);
        assertNotNull(result);
        assertEquals(1, result.size());
        Product brand = result.get(0);
        assertEquals(NIKE, brand.getBrand());
        assertEquals(BOOTS, brand.getName());
    }

    @Test
    void testFindProductsByName() {
        List<Product> result = repository.findProductsByName(MOTHERBOARD);
        assertNotNull(result);
        assertEquals(1, result.size());
        Product name = result.get(0);
        assertEquals(ASUS, name.getBrand());
        assertEquals(MOTHERBOARD, name.getName());
    }

    @Test
    void testNotFoundByName() {
        List<Product> res = repository.findProductsByName("intel");
        assertNotNull(res);
        assertTrue(res.isEmpty());
    }

    @Test
    void testNotFoundByBrand() {
        List<Product> res = repository.findProductsByName("t-shirt");
        assertNotNull(res);
        assertTrue(res.isEmpty());
    }

    private Product getProduct(int cnt, String brand, String name) {
        Product product = new Product();

        product.setId(UUID.randomUUID().toString());
        product.setPrice(BigDecimal.TEN);
        product.setBrand(brand);
        product.setName(name);
        product.setQuantity(cnt);

        return product;
    }
}