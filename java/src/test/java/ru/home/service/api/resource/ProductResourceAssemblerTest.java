package ru.home.service.api.resource;

import org.junit.jupiter.api.Test;
import ru.home.service.domain.Product;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class ProductResourceAssemblerTest {

    @Test
    public void testToResource() {
        Product product = getProduct();

        ProductResource resource = new ProductResourceAssembler().toResource(product);

        assertNotNull(resource);
        assertEquals(product.getBrand(), resource.getBrand());
        assertEquals(product.getName(), resource.getName());
        assertEquals(0, product.getPrice().compareTo(resource.getPrice()));
        assertEquals(product.getQuantity(), resource.getQuantity());
        assertEquals("/product/" + product.getId(), resource.getId().getHref());
    }

    private Product getProduct() {
        Product product = new Product();
        product.setQuantity(1);
        product.setName("NaMe");
        product.setBrand("bRaNd");
        product.setPrice(BigDecimal.TEN);
        product.setId("Id123");
        return product;
    }
}