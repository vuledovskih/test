package ru.home.deposit.service;

import ru.home.deposit.api.DepositDto;

import java.math.BigDecimal;

public interface DepositService {

    String open();

    DepositDto getDeposit(String no);

    void income(String no, BigDecimal amount);

    void transfer(String no, String recipient, BigDecimal amount);

    void withdrawal(String no, BigDecimal amount);
}
