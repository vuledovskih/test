package ru.home.deposit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.home.deposit.api.DepositDto;
import ru.home.deposit.domain.Deposit;
import ru.home.deposit.domain.repository.DepositRepository;
import ru.home.deposit.exception.DepositNotFound;
import ru.home.deposit.exception.NotEnoughMoney;

import java.math.BigDecimal;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
class DepositServiceImpl implements DepositService {

    @Autowired
    private DepositRepository repository;

    @Override
    public void income(String no, BigDecimal amount) {
        Deposit deposit = repository.findByNoAndLock(no);
        deposit.setBalance(deposit.getBalance().add(amount));
    }

    @Override
    public void transfer(String no, String recipient, BigDecimal amount) {
        Deposits lockedDeposit = lockForTransfer(no, recipient);

        validateBalance(lockedDeposit.from, amount);

        lockedDeposit.from.setBalance(lockedDeposit.from.getBalance().subtract(amount));
        lockedDeposit.recipient.setBalance(lockedDeposit.recipient.getBalance().add(amount));
    }

    @Override
    public void withdrawal(String no, BigDecimal amount) {
        Deposit payer = repository.findByNoAndLock(no);
        validateBalance(payer, amount);
        payer.setBalance(payer.getBalance().subtract(amount));
    }

    @Override
    public String open() {
        return repository.create().getNo();
    }

    @Override
    public DepositDto getDeposit(String no) {
        Deposit deposit = repository.findByNo(no);

        if (deposit == null) {
            throw new DepositNotFound(no);
        }

        DepositDto result = new DepositDto();
        result.setAmount(deposit.getBalance());
        result.setNo(deposit.getNo());
        return result;
    }

    private Deposits lockForTransfer(String fromNo, String recipientNo) {
        Deposit from;
        Deposit recipient;
        if (fromNo.compareTo(recipientNo) <= 0) {
            from = repository.findByNoAndLock(fromNo);
            recipient = repository.findByNoAndLock(recipientNo);
        } else {
            recipient = repository.findByNoAndLock(recipientNo);
            from = repository.findByNoAndLock(fromNo);
        }
        return new Deposits(from, recipient);
    }

    private void validateBalance(Deposit payer, BigDecimal amount) {
        if (payer.getBalance().compareTo(amount) < 0) {
            throw new NotEnoughMoney(payer, amount);
        }
    }

    private static class Deposits {
        private final Deposit from;
        private final Deposit recipient;

        private Deposits(Deposit from, Deposit recipient) {
            this.from = from;
            this.recipient = recipient;
        }
    }

}
