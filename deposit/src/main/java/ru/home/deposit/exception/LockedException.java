package ru.home.deposit.exception;

public class LockedException extends RuntimeException {
    public LockedException(String no) {
        super("deposit with no=" + no + " locked");
    }
}
