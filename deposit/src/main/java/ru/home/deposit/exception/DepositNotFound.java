package ru.home.deposit.exception;

public class DepositNotFound extends RuntimeException {
    public DepositNotFound(String no) {
        super("no deposit with number: " + no);
    }
}
