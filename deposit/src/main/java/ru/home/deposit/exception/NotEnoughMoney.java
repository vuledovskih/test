package ru.home.deposit.exception;

import ru.home.deposit.domain.Deposit;

import java.math.BigDecimal;

public class NotEnoughMoney extends RuntimeException {

    public NotEnoughMoney(Deposit payer, BigDecimal required) {
        super("balance is too low, required: " + required + ", deposit: " + payer);
    }
}
