package ru.home.deposit.domain.repository;

import ru.home.deposit.domain.Deposit;

public interface DepositRepository  { //extends CrudRepository {

    Deposit findByNoAndLock(String no);

    Deposit create();

    Deposit findByNo(String no);
}
