package ru.home.deposit.domain.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.home.deposit.domain.Deposit;
import ru.home.deposit.exception.DepositNotFound;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class DepositRepositoryImpl implements DepositRepository {
    private static final Logger logger = LoggerFactory.getLogger(DepositRepositoryImpl.class);

    private static final String PARAM_NO = "no";
    private static final String SELECT_BY_NO = "SELECT d FROM Deposit d WHERE d.no = :" + PARAM_NO;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Deposit findByNoAndLock(String no) {
        logger.info("try lock deposit {}", no);
        //TODO: ловить ошибку таймаута нормально
        try {
            return em.createQuery(SELECT_BY_NO, Deposit.class)
                    .setParameter(PARAM_NO, no)
                    .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                    .getSingleResult();
        } catch (EmptyResultDataAccessException | NoResultException ex) {
            logger.error(ex.getMessage());
            logger.trace(ex.getMessage(), ex);
            throw new DepositNotFound(no);
        }
    }

    @Override
    public Deposit findByNo(String no) {
        List<Deposit> result = em.createQuery(SELECT_BY_NO, Deposit.class)
                .setParameter(PARAM_NO, no)
                .getResultList();
        if (result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            throw new IllegalStateException();
        }
        return result.get(0);
    }

    @Override
    public Deposit create() {
        Deposit deposit = new Deposit();
        deposit.setBalance(BigDecimal.ZERO);
        em.persist(deposit);
        deposit.setNo(String.valueOf(deposit.getId()));
        return deposit;
    }
}
