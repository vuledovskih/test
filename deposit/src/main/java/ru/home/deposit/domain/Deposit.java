package ru.home.deposit.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "DEPOSIT")
public class Deposit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String no;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "id=" + id +
                ", no='" + no + '\'' +
                ", balance=" + balance +
                '}';
    }
}
