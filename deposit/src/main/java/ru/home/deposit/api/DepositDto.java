package ru.home.deposit.api;

import java.math.BigDecimal;

public class DepositDto {

    private String no;
    private BigDecimal amount;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
