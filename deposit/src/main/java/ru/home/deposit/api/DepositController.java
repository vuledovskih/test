package ru.home.deposit.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.home.deposit.service.DepositService;

import java.math.BigDecimal;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.*;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(path = "deposit")
public class DepositController {
    private static final Logger logger = LoggerFactory.getLogger(DepositController.class);

    @Autowired
    private DepositService service;

    @Autowired
    private ErrorProcessor errorProcessor;

    @PutMapping(path = "{no}", consumes = APPLICATION_JSON_UTF8_VALUE)
    public HttpEntity income(@PathVariable("no") String no, @RequestBody BigDecimal amount) {
        try {
            service.income(no, amount);
            return ok().build();
        } catch (RuntimeException lex) {
            return errorProcessor.processException(lex);
        }
    }

    @PostMapping(path = "{no}/transfer", consumes = APPLICATION_JSON_UTF8_VALUE)
    public HttpEntity transfer(@PathVariable("no") String no, @RequestBody TransferDto transfer) {
        try {
            if (isNull(transfer.getAmount()) || isNull(transfer.getRecipientNo())) {
                logger.error("miss required parameter");
                return status(HttpStatus.BAD_REQUEST).build();
            }
            if (no.equals(transfer.getRecipientNo())) {
                logger.error("transfer to same deposit");
                return status(HttpStatus.BAD_REQUEST).build();
            }
            service.transfer(no, transfer.getRecipientNo(), transfer.getAmount());
            return ok().build();
        } catch (RuntimeException rex) {
            return errorProcessor.processException(rex);
        }
    }

    @PostMapping(path = "{no}/withdrawal")
    public HttpEntity withdrawal(@PathVariable("no") String no, @RequestBody BigDecimal amount) {
        try {
            service.withdrawal(no, amount);
            return ok().build();
        } catch (RuntimeException rex) {
            return errorProcessor.processException(rex);
        }
    }

    //TODO: добавить в ответ редирект на GET
    @PutMapping
    public ModelAndView create() {
        return new ModelAndView("redirect:/" + service.open());
    }

    @GetMapping(path = "{no}")
    public HttpEntity<DepositDto> getDeposit(@PathVariable("no") String no) {
        try {
            return ok(service.getDeposit(no));
        } catch (RuntimeException rex) {
            return (HttpEntity<DepositDto> )errorProcessor.processException(rex);
        }
    }
}
