package ru.home.deposit.api;

import java.math.BigDecimal;

public class TransferDto {
    private String recipientNo;
    private BigDecimal amount;

    public String getRecipientNo() {
        return recipientNo;
    }

    public void setRecipientNo(String recipientNo) {
        this.recipientNo = recipientNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransferDto{" +
                "recipientNo='" + recipientNo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
