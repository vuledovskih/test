package ru.home.deposit.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import ru.home.deposit.exception.DepositNotFound;
import ru.home.deposit.exception.LockedException;
import ru.home.deposit.exception.NotEnoughMoney;

import static org.springframework.http.ResponseEntity.status;

@Component
public class ErrorProcessor {

    HttpEntity processException(RuntimeException rex) {
        if (rex instanceof DepositNotFound) {
            return status(HttpStatus.BAD_REQUEST).build();
        }
        if (rex instanceof LockedException) {
            return status(HttpStatus.CONFLICT).build();
        }
        if (rex instanceof NotEnoughMoney) {
            return status(HttpStatus.PAYMENT_REQUIRED).build();
        }
        return status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
