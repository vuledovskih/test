package ru.home.deposit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.home.deposit.aop.AopLogger;

@Configuration
public class AppConfiguration {

    @Bean
    public AopLogger aopLogger() {
        return new AopLogger();
    }

}
