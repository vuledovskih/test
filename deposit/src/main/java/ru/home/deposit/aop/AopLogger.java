package ru.home.deposit.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class AopLogger {

    private static final Logger logger = LoggerFactory.getLogger(AopLogger.class);

    @Pointcut("execution(public * ru.home.deposit.api.*.*(..))")
    public void logController() {
    }

    @Pointcut("execution(public * ru.home.deposit.service.*.*(..))")
    public void logService() {
    }

    @Around("logController()")
    public Object aroundControllerLogableMethod(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getStaticPart().getSignature();
        logger.info("==> {}.{}({})", signature.getDeclaringTypeName(), signature.getName(), point.getArgs());

        try {
            Object result = point.proceed();
            logger.debug("result: {}", result);
            logger.info("<== {}.{}() success", signature.getDeclaringTypeName(), signature.getName());
            return result;
        } catch (Throwable th) {
            logger.error("fail execute " + signature.getDeclaringTypeName() + "." + signature.getName(), th);
            throw th;
        }
    }

    @Around("logService()")
    public Object aroundServiceLogableMethod(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getStaticPart().getSignature();
        logger.debug("==> {}.{}({})", signature.getDeclaringTypeName(), signature.getName(), point.getArgs());

        try {
            Object result = point.proceed();
            logger.debug("<== {}.{}() result{}", signature.getDeclaringTypeName(), signature.getName(), result);
            return result;
        } catch (Throwable th) {
            logger.error("fail execute " + signature.getDeclaringTypeName() + "." + signature.getName());
            logger.trace(th.getMessage(), th);
            throw th;
        }
    }
}
