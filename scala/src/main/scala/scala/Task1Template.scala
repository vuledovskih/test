package scala

import org.apache.spark.sql.{Row, SaveMode, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.functions._


object Task1Template extends App {

  val schema: StructType = {
    StructType(
      Seq(
        StructField(name = "line", dataType = StringType, nullable = false)
      )
    )
  }

  def extractPartNum = udf((value: String) => {
    def isNumeric(s: String): Boolean = {
      if (s.isEmpty) false
      else s forall Character.isDigit
    }
    value.split("\\s").reduce((r, r1) => if (isNumeric(r)) r else r1)
  })

  def arrayAvr(arr: Array[Int]): Int = {
    arr.sum / arr.length
  }

  val sparkSession: SparkSession = SparkSession.builder.master("local").enableHiveSupport().getOrCreate()
  val sc = sparkSession.sparkContext

  // Read file as RDD[String]
  val textFile = sc.textFile("input.log")


  // Creates a DataFrame from RDD[String] having a single column named "line"
  val df = sparkSession.createDataFrame(textFile.map(f => Row(f)), schema)

  // Using DataFrame API get all error messages. (Contains "error" word. case-insensitive)
  val errors = df.filter(lower(df("line")).contains("error"))
  // Count them
  val errorsCnt = errors.count()
  //  errors.explain(true)

  // Count messages from errors that contain java (java word)
  val javaEr = errors.filter(errors("line").contains("java")) count()

  // Fetches the Exception messages from initial DataFrame as an array of strings (Exception word)
  //TODO: "Exception:" включая или нет?
  val exceptionAr = df.filter(col("line")
    .contains("Exception:"))
    .withColumn("line",
      col("line").substr(locate("Exception", col("line")), length(col("line")))
    )
  //  exceptionAr.explain(true)
  //  exceptionAr.show()

  // Parse messages in array of exceptions, extract partitions numbers and save them in array  ("partition" is in log file)
  val partitionsAr = exceptionAr.withColumn("line", extractPartNum(col("line")))

  //  partitionsAr.explain(true)
  //  partitionsAr.show()

  val minPartition = partitionsAr.agg({
    "line" -> "min"
  })
  val maxPartition = partitionsAr.agg({
    "line" -> "max"
  })
  //  minPartition.explain(true)
  //  minPartition.show()
  //  maxPartition.show()

  // See array of partitions as usual array of ints.
  // Find average value
  val avrPartition = arrayAvr(partitionsAr.select(col("line")).collect().map(_.getString(0)).map(_.toInt))

  // Write array of partitions in 5 files using ORC format (use DataFrame)
  //    ???
  partitionsAr.repartition(5).write.mode(SaveMode.Overwrite).format("orc").save("./tmp/")

  //todo: Write unit tests
}
